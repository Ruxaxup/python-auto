import time
import win32gui
from win10toast import ToastNotifier
from playsound import playsound #pip install playsound==1.2.2
from slack_sdk import WebClient
# win32api
import win32api
# SlackApiError
from slack_sdk.errors import SlackApiError
client = WebClient(token='xoxb-1093785811472-2459980479715-nWJGCDSzQB05wTXKcZOMD0i3')

PLAYER_NAME = 'Mermelada'
SLACK_ID = 'U011V66FQ6A'

notificar400 = False
notificar100 = False
notificar50 = False
notificar220 = False
notificar350 = False

# create a sleep function in seconds
def sleep(seconds):
    time.sleep(seconds)
    

toplist, winlist = [], []

def enum_cb(hwnd, results):
    winlist.append((hwnd, win32gui.GetWindowText(hwnd)))

def getLevel(title):    
    # find index of Level string in title
    print('title::'+title)
    lvl_offset = 6
    level_index = title.find('Level')
    if level_index == -1:
        level_index = title.find('Lvl')
        lvl_offset = 3
    ml_index = title.find('+')
    
    # get text between leve_index and ml_index, trim it and convert to int
    level = int(title[level_index + lvl_offset:ml_index].strip())    
    
    return level

def notifyLevelReached(level):
    toaster = ToastNotifier()
    toaster.show_toast("Level Reached", "You have reached level " + str(level), duration=60)
    # Input an existing wav filename
    playsound('./notification_sound.mp3', False)

    try:
        response = client.chat_postMessage(
        channel=SLACK_ID,
        text= "You have reached level " + str(level))
    except SlackApiError as e:
        # You will get a SlackApiError if "ok" is False
        print(f"Got an error: {e.response['error']}")
    
    # display a popup window
    win32api.MessageBox(0, 'You have reached level ' + str(level), 'Level Reached', 0x00001000)


if __name__ == '__main__':
    while True:
        win32gui.EnumWindows(enum_cb, toplist)
        # filter window with EPICMU title
        epicmu = [(hwnd, title) for hwnd, title in winlist if PLAYER_NAME in title]

        # if epicmu lenght is 0, repeat loop
        if len(epicmu) == 0:
            sleep(10)
            continue

        # just grab the hwnd for first window matching EPICMU title
        epicmu = epicmu[0]
        print(epicmu)
        title = epicmu[1]

        level = getLevel(title)
        print('Current level::' + str(level))

        if level >= 400 and not notificar400:
            notifyLevelReached(level)
            notificar400 = True
        elif level >= 350 and not notificar350:
            notifyLevelReached(level)
            notificar350 = True
        elif level >= 220 and not notificar220:
            notifyLevelReached(level)
            notificar220 = True
        elif level >= 100 and not notificar100:
            notifyLevelReached(level)
            notificar100 = True
        elif level >= 50 and not notificar100:
            notifyLevelReached(level)
            notificar50 = True    
        elif level == 1:
            notificar50 = False
            notificar100 = False
            notificar220 = False
            notificar350 = False
            notificar400 = False

        sleep(10)
        # clear winlist
        winlist = []